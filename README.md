# TemplateKivyMVC

templatekivymvc is a python script to create a kivy project structure with the mvc design pattern.

## Usage

# create a project in the indicated path
python3 main.py path_project name_project


## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
